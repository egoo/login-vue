import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
Vue.use(Vuex)

const state = {
  about: []
}
const actions = {
  async GetData ({ commit }) {
    const { response } = await axios.get('https://www.remax.co.id/prodigy/papi/webabout')
    commit('LOAD_ABOUT', { list: response.data.data })
  }
}
const mutation = {
  LOAD_ABOUT (state, { list }) {
    state.about = list
  }
}
const getters = {
  List(state) {
    return state.about
  }
}
const store = new Vuex.Store({
  state,
  mutation,
  actions,
  getters
})

export default store
